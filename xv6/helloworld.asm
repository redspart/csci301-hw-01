
_helloworld:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "types.h"
#include "stat.h"
#include "user.h"

int main(int argc, char *argv[])
{
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	83 e4 f0             	and    $0xfffffff0,%esp
   6:	83 ec 20             	sub    $0x20,%esp
    int i;
    
    for(i=1; i < 2; i++)
   9:	c7 44 24 1c 01 00 00 	movl   $0x1,0x1c(%esp)
  10:	00 
  11:	eb 19                	jmp    2c <main+0x2c>
       printf(1, "Hello, world!\n"); 
  13:	c7 44 24 04 df 07 00 	movl   $0x7df,0x4(%esp)
  1a:	00 
  1b:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  22:	e8 f1 03 00 00       	call   418 <printf>

int main(int argc, char *argv[])
{
    int i;
    
    for(i=1; i < 2; i++)
  27:	83 44 24 1c 01       	addl   $0x1,0x1c(%esp)
  2c:	83 7c 24 1c 01       	cmpl   $0x1,0x1c(%esp)
  31:	7e e0                	jle    13 <main+0x13>
       printf(1, "Hello, world!\n"); 
    exit();
  33:	e8 64 02 00 00       	call   29c <exit>

00000038 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
  38:	55                   	push   %ebp
  39:	89 e5                	mov    %esp,%ebp
  3b:	57                   	push   %edi
  3c:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
  3d:	8b 4d 08             	mov    0x8(%ebp),%ecx
  40:	8b 55 10             	mov    0x10(%ebp),%edx
  43:	8b 45 0c             	mov    0xc(%ebp),%eax
  46:	89 cb                	mov    %ecx,%ebx
  48:	89 df                	mov    %ebx,%edi
  4a:	89 d1                	mov    %edx,%ecx
  4c:	fc                   	cld    
  4d:	f3 aa                	rep stos %al,%es:(%edi)
  4f:	89 ca                	mov    %ecx,%edx
  51:	89 fb                	mov    %edi,%ebx
  53:	89 5d 08             	mov    %ebx,0x8(%ebp)
  56:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
  59:	5b                   	pop    %ebx
  5a:	5f                   	pop    %edi
  5b:	5d                   	pop    %ebp
  5c:	c3                   	ret    

0000005d <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
  5d:	55                   	push   %ebp
  5e:	89 e5                	mov    %esp,%ebp
  60:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
  63:	8b 45 08             	mov    0x8(%ebp),%eax
  66:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
  69:	8b 45 0c             	mov    0xc(%ebp),%eax
  6c:	0f b6 10             	movzbl (%eax),%edx
  6f:	8b 45 08             	mov    0x8(%ebp),%eax
  72:	88 10                	mov    %dl,(%eax)
  74:	8b 45 08             	mov    0x8(%ebp),%eax
  77:	0f b6 00             	movzbl (%eax),%eax
  7a:	84 c0                	test   %al,%al
  7c:	0f 95 c0             	setne  %al
  7f:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  83:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
  87:	84 c0                	test   %al,%al
  89:	75 de                	jne    69 <strcpy+0xc>
    ;
  return os;
  8b:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  8e:	c9                   	leave  
  8f:	c3                   	ret    

00000090 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  90:	55                   	push   %ebp
  91:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
  93:	eb 08                	jmp    9d <strcmp+0xd>
    p++, q++;
  95:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  99:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
  9d:	8b 45 08             	mov    0x8(%ebp),%eax
  a0:	0f b6 00             	movzbl (%eax),%eax
  a3:	84 c0                	test   %al,%al
  a5:	74 10                	je     b7 <strcmp+0x27>
  a7:	8b 45 08             	mov    0x8(%ebp),%eax
  aa:	0f b6 10             	movzbl (%eax),%edx
  ad:	8b 45 0c             	mov    0xc(%ebp),%eax
  b0:	0f b6 00             	movzbl (%eax),%eax
  b3:	38 c2                	cmp    %al,%dl
  b5:	74 de                	je     95 <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
  b7:	8b 45 08             	mov    0x8(%ebp),%eax
  ba:	0f b6 00             	movzbl (%eax),%eax
  bd:	0f b6 d0             	movzbl %al,%edx
  c0:	8b 45 0c             	mov    0xc(%ebp),%eax
  c3:	0f b6 00             	movzbl (%eax),%eax
  c6:	0f b6 c0             	movzbl %al,%eax
  c9:	89 d1                	mov    %edx,%ecx
  cb:	29 c1                	sub    %eax,%ecx
  cd:	89 c8                	mov    %ecx,%eax
}
  cf:	5d                   	pop    %ebp
  d0:	c3                   	ret    

000000d1 <strlen>:

uint
strlen(char *s)
{
  d1:	55                   	push   %ebp
  d2:	89 e5                	mov    %esp,%ebp
  d4:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
  d7:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  de:	eb 04                	jmp    e4 <strlen+0x13>
  e0:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
  e4:	8b 45 fc             	mov    -0x4(%ebp),%eax
  e7:	03 45 08             	add    0x8(%ebp),%eax
  ea:	0f b6 00             	movzbl (%eax),%eax
  ed:	84 c0                	test   %al,%al
  ef:	75 ef                	jne    e0 <strlen+0xf>
    ;
  return n;
  f1:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  f4:	c9                   	leave  
  f5:	c3                   	ret    

000000f6 <memset>:

void*
memset(void *dst, int c, uint n)
{
  f6:	55                   	push   %ebp
  f7:	89 e5                	mov    %esp,%ebp
  f9:	83 ec 0c             	sub    $0xc,%esp
  stosb(dst, c, n);
  fc:	8b 45 10             	mov    0x10(%ebp),%eax
  ff:	89 44 24 08          	mov    %eax,0x8(%esp)
 103:	8b 45 0c             	mov    0xc(%ebp),%eax
 106:	89 44 24 04          	mov    %eax,0x4(%esp)
 10a:	8b 45 08             	mov    0x8(%ebp),%eax
 10d:	89 04 24             	mov    %eax,(%esp)
 110:	e8 23 ff ff ff       	call   38 <stosb>
  return dst;
 115:	8b 45 08             	mov    0x8(%ebp),%eax
}
 118:	c9                   	leave  
 119:	c3                   	ret    

0000011a <strchr>:

char*
strchr(const char *s, char c)
{
 11a:	55                   	push   %ebp
 11b:	89 e5                	mov    %esp,%ebp
 11d:	83 ec 04             	sub    $0x4,%esp
 120:	8b 45 0c             	mov    0xc(%ebp),%eax
 123:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 126:	eb 14                	jmp    13c <strchr+0x22>
    if(*s == c)
 128:	8b 45 08             	mov    0x8(%ebp),%eax
 12b:	0f b6 00             	movzbl (%eax),%eax
 12e:	3a 45 fc             	cmp    -0x4(%ebp),%al
 131:	75 05                	jne    138 <strchr+0x1e>
      return (char*)s;
 133:	8b 45 08             	mov    0x8(%ebp),%eax
 136:	eb 13                	jmp    14b <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 138:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 13c:	8b 45 08             	mov    0x8(%ebp),%eax
 13f:	0f b6 00             	movzbl (%eax),%eax
 142:	84 c0                	test   %al,%al
 144:	75 e2                	jne    128 <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 146:	b8 00 00 00 00       	mov    $0x0,%eax
}
 14b:	c9                   	leave  
 14c:	c3                   	ret    

0000014d <gets>:

char*
gets(char *buf, int max)
{
 14d:	55                   	push   %ebp
 14e:	89 e5                	mov    %esp,%ebp
 150:	83 ec 28             	sub    $0x28,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 153:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 15a:	eb 44                	jmp    1a0 <gets+0x53>
    cc = read(0, &c, 1);
 15c:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 163:	00 
 164:	8d 45 ef             	lea    -0x11(%ebp),%eax
 167:	89 44 24 04          	mov    %eax,0x4(%esp)
 16b:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 172:	e8 3d 01 00 00       	call   2b4 <read>
 177:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(cc < 1)
 17a:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 17e:	7e 2d                	jle    1ad <gets+0x60>
      break;
    buf[i++] = c;
 180:	8b 45 f0             	mov    -0x10(%ebp),%eax
 183:	03 45 08             	add    0x8(%ebp),%eax
 186:	0f b6 55 ef          	movzbl -0x11(%ebp),%edx
 18a:	88 10                	mov    %dl,(%eax)
 18c:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
    if(c == '\n' || c == '\r')
 190:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 194:	3c 0a                	cmp    $0xa,%al
 196:	74 16                	je     1ae <gets+0x61>
 198:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 19c:	3c 0d                	cmp    $0xd,%al
 19e:	74 0e                	je     1ae <gets+0x61>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 1a0:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1a3:	83 c0 01             	add    $0x1,%eax
 1a6:	3b 45 0c             	cmp    0xc(%ebp),%eax
 1a9:	7c b1                	jl     15c <gets+0xf>
 1ab:	eb 01                	jmp    1ae <gets+0x61>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 1ad:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 1ae:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1b1:	03 45 08             	add    0x8(%ebp),%eax
 1b4:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 1b7:	8b 45 08             	mov    0x8(%ebp),%eax
}
 1ba:	c9                   	leave  
 1bb:	c3                   	ret    

000001bc <stat>:

int
stat(char *n, struct stat *st)
{
 1bc:	55                   	push   %ebp
 1bd:	89 e5                	mov    %esp,%ebp
 1bf:	83 ec 28             	sub    $0x28,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 1c2:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 1c9:	00 
 1ca:	8b 45 08             	mov    0x8(%ebp),%eax
 1cd:	89 04 24             	mov    %eax,(%esp)
 1d0:	e8 07 01 00 00       	call   2dc <open>
 1d5:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(fd < 0)
 1d8:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 1dc:	79 07                	jns    1e5 <stat+0x29>
    return -1;
 1de:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 1e3:	eb 23                	jmp    208 <stat+0x4c>
  r = fstat(fd, st);
 1e5:	8b 45 0c             	mov    0xc(%ebp),%eax
 1e8:	89 44 24 04          	mov    %eax,0x4(%esp)
 1ec:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1ef:	89 04 24             	mov    %eax,(%esp)
 1f2:	e8 fd 00 00 00       	call   2f4 <fstat>
 1f7:	89 45 f4             	mov    %eax,-0xc(%ebp)
  close(fd);
 1fa:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1fd:	89 04 24             	mov    %eax,(%esp)
 200:	e8 bf 00 00 00       	call   2c4 <close>
  return r;
 205:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
 208:	c9                   	leave  
 209:	c3                   	ret    

0000020a <atoi>:

int
atoi(const char *s)
{
 20a:	55                   	push   %ebp
 20b:	89 e5                	mov    %esp,%ebp
 20d:	83 ec 10             	sub    $0x10,%esp
  int n;

  n = 0;
 210:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while('0' <= *s && *s <= '9')
 217:	eb 24                	jmp    23d <atoi+0x33>
    n = n*10 + *s++ - '0';
 219:	8b 55 fc             	mov    -0x4(%ebp),%edx
 21c:	89 d0                	mov    %edx,%eax
 21e:	c1 e0 02             	shl    $0x2,%eax
 221:	01 d0                	add    %edx,%eax
 223:	01 c0                	add    %eax,%eax
 225:	89 c2                	mov    %eax,%edx
 227:	8b 45 08             	mov    0x8(%ebp),%eax
 22a:	0f b6 00             	movzbl (%eax),%eax
 22d:	0f be c0             	movsbl %al,%eax
 230:	8d 04 02             	lea    (%edx,%eax,1),%eax
 233:	83 e8 30             	sub    $0x30,%eax
 236:	89 45 fc             	mov    %eax,-0x4(%ebp)
 239:	83 45 08 01          	addl   $0x1,0x8(%ebp)
atoi(const char *s)
{
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 23d:	8b 45 08             	mov    0x8(%ebp),%eax
 240:	0f b6 00             	movzbl (%eax),%eax
 243:	3c 2f                	cmp    $0x2f,%al
 245:	7e 0a                	jle    251 <atoi+0x47>
 247:	8b 45 08             	mov    0x8(%ebp),%eax
 24a:	0f b6 00             	movzbl (%eax),%eax
 24d:	3c 39                	cmp    $0x39,%al
 24f:	7e c8                	jle    219 <atoi+0xf>
    n = n*10 + *s++ - '0';
  return n;
 251:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 254:	c9                   	leave  
 255:	c3                   	ret    

00000256 <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 256:	55                   	push   %ebp
 257:	89 e5                	mov    %esp,%ebp
 259:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 25c:	8b 45 08             	mov    0x8(%ebp),%eax
 25f:	89 45 f8             	mov    %eax,-0x8(%ebp)
  src = vsrc;
 262:	8b 45 0c             	mov    0xc(%ebp),%eax
 265:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(n-- > 0)
 268:	eb 13                	jmp    27d <memmove+0x27>
    *dst++ = *src++;
 26a:	8b 45 fc             	mov    -0x4(%ebp),%eax
 26d:	0f b6 10             	movzbl (%eax),%edx
 270:	8b 45 f8             	mov    -0x8(%ebp),%eax
 273:	88 10                	mov    %dl,(%eax)
 275:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
 279:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 27d:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
 281:	0f 9f c0             	setg   %al
 284:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
 288:	84 c0                	test   %al,%al
 28a:	75 de                	jne    26a <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 28c:	8b 45 08             	mov    0x8(%ebp),%eax
}
 28f:	c9                   	leave  
 290:	c3                   	ret    
 291:	90                   	nop
 292:	90                   	nop
 293:	90                   	nop

00000294 <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 294:	b8 01 00 00 00       	mov    $0x1,%eax
 299:	cd 40                	int    $0x40
 29b:	c3                   	ret    

0000029c <exit>:
SYSCALL(exit)
 29c:	b8 02 00 00 00       	mov    $0x2,%eax
 2a1:	cd 40                	int    $0x40
 2a3:	c3                   	ret    

000002a4 <wait>:
SYSCALL(wait)
 2a4:	b8 03 00 00 00       	mov    $0x3,%eax
 2a9:	cd 40                	int    $0x40
 2ab:	c3                   	ret    

000002ac <pipe>:
SYSCALL(pipe)
 2ac:	b8 04 00 00 00       	mov    $0x4,%eax
 2b1:	cd 40                	int    $0x40
 2b3:	c3                   	ret    

000002b4 <read>:
SYSCALL(read)
 2b4:	b8 05 00 00 00       	mov    $0x5,%eax
 2b9:	cd 40                	int    $0x40
 2bb:	c3                   	ret    

000002bc <write>:
SYSCALL(write)
 2bc:	b8 10 00 00 00       	mov    $0x10,%eax
 2c1:	cd 40                	int    $0x40
 2c3:	c3                   	ret    

000002c4 <close>:
SYSCALL(close)
 2c4:	b8 15 00 00 00       	mov    $0x15,%eax
 2c9:	cd 40                	int    $0x40
 2cb:	c3                   	ret    

000002cc <kill>:
SYSCALL(kill)
 2cc:	b8 06 00 00 00       	mov    $0x6,%eax
 2d1:	cd 40                	int    $0x40
 2d3:	c3                   	ret    

000002d4 <exec>:
SYSCALL(exec)
 2d4:	b8 07 00 00 00       	mov    $0x7,%eax
 2d9:	cd 40                	int    $0x40
 2db:	c3                   	ret    

000002dc <open>:
SYSCALL(open)
 2dc:	b8 0f 00 00 00       	mov    $0xf,%eax
 2e1:	cd 40                	int    $0x40
 2e3:	c3                   	ret    

000002e4 <mknod>:
SYSCALL(mknod)
 2e4:	b8 11 00 00 00       	mov    $0x11,%eax
 2e9:	cd 40                	int    $0x40
 2eb:	c3                   	ret    

000002ec <unlink>:
SYSCALL(unlink)
 2ec:	b8 12 00 00 00       	mov    $0x12,%eax
 2f1:	cd 40                	int    $0x40
 2f3:	c3                   	ret    

000002f4 <fstat>:
SYSCALL(fstat)
 2f4:	b8 08 00 00 00       	mov    $0x8,%eax
 2f9:	cd 40                	int    $0x40
 2fb:	c3                   	ret    

000002fc <link>:
SYSCALL(link)
 2fc:	b8 13 00 00 00       	mov    $0x13,%eax
 301:	cd 40                	int    $0x40
 303:	c3                   	ret    

00000304 <mkdir>:
SYSCALL(mkdir)
 304:	b8 14 00 00 00       	mov    $0x14,%eax
 309:	cd 40                	int    $0x40
 30b:	c3                   	ret    

0000030c <chdir>:
SYSCALL(chdir)
 30c:	b8 09 00 00 00       	mov    $0x9,%eax
 311:	cd 40                	int    $0x40
 313:	c3                   	ret    

00000314 <dup>:
SYSCALL(dup)
 314:	b8 0a 00 00 00       	mov    $0xa,%eax
 319:	cd 40                	int    $0x40
 31b:	c3                   	ret    

0000031c <getpid>:
SYSCALL(getpid)
 31c:	b8 0b 00 00 00       	mov    $0xb,%eax
 321:	cd 40                	int    $0x40
 323:	c3                   	ret    

00000324 <sbrk>:
SYSCALL(sbrk)
 324:	b8 0c 00 00 00       	mov    $0xc,%eax
 329:	cd 40                	int    $0x40
 32b:	c3                   	ret    

0000032c <sleep>:
SYSCALL(sleep)
 32c:	b8 0d 00 00 00       	mov    $0xd,%eax
 331:	cd 40                	int    $0x40
 333:	c3                   	ret    

00000334 <uptime>:
SYSCALL(uptime)
 334:	b8 0e 00 00 00       	mov    $0xe,%eax
 339:	cd 40                	int    $0x40
 33b:	c3                   	ret    

0000033c <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 33c:	55                   	push   %ebp
 33d:	89 e5                	mov    %esp,%ebp
 33f:	83 ec 28             	sub    $0x28,%esp
 342:	8b 45 0c             	mov    0xc(%ebp),%eax
 345:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 348:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 34f:	00 
 350:	8d 45 f4             	lea    -0xc(%ebp),%eax
 353:	89 44 24 04          	mov    %eax,0x4(%esp)
 357:	8b 45 08             	mov    0x8(%ebp),%eax
 35a:	89 04 24             	mov    %eax,(%esp)
 35d:	e8 5a ff ff ff       	call   2bc <write>
}
 362:	c9                   	leave  
 363:	c3                   	ret    

00000364 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 364:	55                   	push   %ebp
 365:	89 e5                	mov    %esp,%ebp
 367:	53                   	push   %ebx
 368:	83 ec 44             	sub    $0x44,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 36b:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 372:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 376:	74 17                	je     38f <printint+0x2b>
 378:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 37c:	79 11                	jns    38f <printint+0x2b>
    neg = 1;
 37e:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 385:	8b 45 0c             	mov    0xc(%ebp),%eax
 388:	f7 d8                	neg    %eax
 38a:	89 45 f4             	mov    %eax,-0xc(%ebp)
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
  if(sgn && xx < 0){
 38d:	eb 06                	jmp    395 <printint+0x31>
    neg = 1;
    x = -xx;
  } else {
    x = xx;
 38f:	8b 45 0c             	mov    0xc(%ebp),%eax
 392:	89 45 f4             	mov    %eax,-0xc(%ebp)
  }

  i = 0;
 395:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  do{
    buf[i++] = digits[x % base];
 39c:	8b 4d ec             	mov    -0x14(%ebp),%ecx
 39f:	8b 5d 10             	mov    0x10(%ebp),%ebx
 3a2:	8b 45 f4             	mov    -0xc(%ebp),%eax
 3a5:	ba 00 00 00 00       	mov    $0x0,%edx
 3aa:	f7 f3                	div    %ebx
 3ac:	89 d0                	mov    %edx,%eax
 3ae:	0f b6 80 f8 07 00 00 	movzbl 0x7f8(%eax),%eax
 3b5:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
 3b9:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
  }while((x /= base) != 0);
 3bd:	8b 45 10             	mov    0x10(%ebp),%eax
 3c0:	89 45 d4             	mov    %eax,-0x2c(%ebp)
 3c3:	8b 45 f4             	mov    -0xc(%ebp),%eax
 3c6:	ba 00 00 00 00       	mov    $0x0,%edx
 3cb:	f7 75 d4             	divl   -0x2c(%ebp)
 3ce:	89 45 f4             	mov    %eax,-0xc(%ebp)
 3d1:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 3d5:	75 c5                	jne    39c <printint+0x38>
  if(neg)
 3d7:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 3db:	74 2a                	je     407 <printint+0xa3>
    buf[i++] = '-';
 3dd:	8b 45 ec             	mov    -0x14(%ebp),%eax
 3e0:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)
 3e5:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)

  while(--i >= 0)
 3e9:	eb 1d                	jmp    408 <printint+0xa4>
    putc(fd, buf[i]);
 3eb:	8b 45 ec             	mov    -0x14(%ebp),%eax
 3ee:	0f b6 44 05 dc       	movzbl -0x24(%ebp,%eax,1),%eax
 3f3:	0f be c0             	movsbl %al,%eax
 3f6:	89 44 24 04          	mov    %eax,0x4(%esp)
 3fa:	8b 45 08             	mov    0x8(%ebp),%eax
 3fd:	89 04 24             	mov    %eax,(%esp)
 400:	e8 37 ff ff ff       	call   33c <putc>
 405:	eb 01                	jmp    408 <printint+0xa4>
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 407:	90                   	nop
 408:	83 6d ec 01          	subl   $0x1,-0x14(%ebp)
 40c:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 410:	79 d9                	jns    3eb <printint+0x87>
    putc(fd, buf[i]);
}
 412:	83 c4 44             	add    $0x44,%esp
 415:	5b                   	pop    %ebx
 416:	5d                   	pop    %ebp
 417:	c3                   	ret    

00000418 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 418:	55                   	push   %ebp
 419:	89 e5                	mov    %esp,%ebp
 41b:	83 ec 38             	sub    $0x38,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 41e:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 425:	8d 45 0c             	lea    0xc(%ebp),%eax
 428:	83 c0 04             	add    $0x4,%eax
 42b:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(i = 0; fmt[i]; i++){
 42e:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
 435:	e9 7e 01 00 00       	jmp    5b8 <printf+0x1a0>
    c = fmt[i] & 0xff;
 43a:	8b 55 0c             	mov    0xc(%ebp),%edx
 43d:	8b 45 ec             	mov    -0x14(%ebp),%eax
 440:	8d 04 02             	lea    (%edx,%eax,1),%eax
 443:	0f b6 00             	movzbl (%eax),%eax
 446:	0f be c0             	movsbl %al,%eax
 449:	25 ff 00 00 00       	and    $0xff,%eax
 44e:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(state == 0){
 451:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 455:	75 2c                	jne    483 <printf+0x6b>
      if(c == '%'){
 457:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 45b:	75 0c                	jne    469 <printf+0x51>
        state = '%';
 45d:	c7 45 f0 25 00 00 00 	movl   $0x25,-0x10(%ebp)
 464:	e9 4b 01 00 00       	jmp    5b4 <printf+0x19c>
      } else {
        putc(fd, c);
 469:	8b 45 e8             	mov    -0x18(%ebp),%eax
 46c:	0f be c0             	movsbl %al,%eax
 46f:	89 44 24 04          	mov    %eax,0x4(%esp)
 473:	8b 45 08             	mov    0x8(%ebp),%eax
 476:	89 04 24             	mov    %eax,(%esp)
 479:	e8 be fe ff ff       	call   33c <putc>
 47e:	e9 31 01 00 00       	jmp    5b4 <printf+0x19c>
      }
    } else if(state == '%'){
 483:	83 7d f0 25          	cmpl   $0x25,-0x10(%ebp)
 487:	0f 85 27 01 00 00    	jne    5b4 <printf+0x19c>
      if(c == 'd'){
 48d:	83 7d e8 64          	cmpl   $0x64,-0x18(%ebp)
 491:	75 2d                	jne    4c0 <printf+0xa8>
        printint(fd, *ap, 10, 1);
 493:	8b 45 f4             	mov    -0xc(%ebp),%eax
 496:	8b 00                	mov    (%eax),%eax
 498:	c7 44 24 0c 01 00 00 	movl   $0x1,0xc(%esp)
 49f:	00 
 4a0:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
 4a7:	00 
 4a8:	89 44 24 04          	mov    %eax,0x4(%esp)
 4ac:	8b 45 08             	mov    0x8(%ebp),%eax
 4af:	89 04 24             	mov    %eax,(%esp)
 4b2:	e8 ad fe ff ff       	call   364 <printint>
        ap++;
 4b7:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 4bb:	e9 ed 00 00 00       	jmp    5ad <printf+0x195>
      } else if(c == 'x' || c == 'p'){
 4c0:	83 7d e8 78          	cmpl   $0x78,-0x18(%ebp)
 4c4:	74 06                	je     4cc <printf+0xb4>
 4c6:	83 7d e8 70          	cmpl   $0x70,-0x18(%ebp)
 4ca:	75 2d                	jne    4f9 <printf+0xe1>
        printint(fd, *ap, 16, 0);
 4cc:	8b 45 f4             	mov    -0xc(%ebp),%eax
 4cf:	8b 00                	mov    (%eax),%eax
 4d1:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
 4d8:	00 
 4d9:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
 4e0:	00 
 4e1:	89 44 24 04          	mov    %eax,0x4(%esp)
 4e5:	8b 45 08             	mov    0x8(%ebp),%eax
 4e8:	89 04 24             	mov    %eax,(%esp)
 4eb:	e8 74 fe ff ff       	call   364 <printint>
        ap++;
 4f0:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
      }
    } else if(state == '%'){
      if(c == 'd'){
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 4f4:	e9 b4 00 00 00       	jmp    5ad <printf+0x195>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 4f9:	83 7d e8 73          	cmpl   $0x73,-0x18(%ebp)
 4fd:	75 46                	jne    545 <printf+0x12d>
        s = (char*)*ap;
 4ff:	8b 45 f4             	mov    -0xc(%ebp),%eax
 502:	8b 00                	mov    (%eax),%eax
 504:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        ap++;
 507:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
        if(s == 0)
 50b:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 50f:	75 27                	jne    538 <printf+0x120>
          s = "(null)";
 511:	c7 45 e4 ee 07 00 00 	movl   $0x7ee,-0x1c(%ebp)
        while(*s != 0){
 518:	eb 1f                	jmp    539 <printf+0x121>
          putc(fd, *s);
 51a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 51d:	0f b6 00             	movzbl (%eax),%eax
 520:	0f be c0             	movsbl %al,%eax
 523:	89 44 24 04          	mov    %eax,0x4(%esp)
 527:	8b 45 08             	mov    0x8(%ebp),%eax
 52a:	89 04 24             	mov    %eax,(%esp)
 52d:	e8 0a fe ff ff       	call   33c <putc>
          s++;
 532:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
 536:	eb 01                	jmp    539 <printf+0x121>
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 538:	90                   	nop
 539:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 53c:	0f b6 00             	movzbl (%eax),%eax
 53f:	84 c0                	test   %al,%al
 541:	75 d7                	jne    51a <printf+0x102>
 543:	eb 68                	jmp    5ad <printf+0x195>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 545:	83 7d e8 63          	cmpl   $0x63,-0x18(%ebp)
 549:	75 1d                	jne    568 <printf+0x150>
        putc(fd, *ap);
 54b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 54e:	8b 00                	mov    (%eax),%eax
 550:	0f be c0             	movsbl %al,%eax
 553:	89 44 24 04          	mov    %eax,0x4(%esp)
 557:	8b 45 08             	mov    0x8(%ebp),%eax
 55a:	89 04 24             	mov    %eax,(%esp)
 55d:	e8 da fd ff ff       	call   33c <putc>
        ap++;
 562:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 566:	eb 45                	jmp    5ad <printf+0x195>
      } else if(c == '%'){
 568:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 56c:	75 17                	jne    585 <printf+0x16d>
        putc(fd, c);
 56e:	8b 45 e8             	mov    -0x18(%ebp),%eax
 571:	0f be c0             	movsbl %al,%eax
 574:	89 44 24 04          	mov    %eax,0x4(%esp)
 578:	8b 45 08             	mov    0x8(%ebp),%eax
 57b:	89 04 24             	mov    %eax,(%esp)
 57e:	e8 b9 fd ff ff       	call   33c <putc>
 583:	eb 28                	jmp    5ad <printf+0x195>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 585:	c7 44 24 04 25 00 00 	movl   $0x25,0x4(%esp)
 58c:	00 
 58d:	8b 45 08             	mov    0x8(%ebp),%eax
 590:	89 04 24             	mov    %eax,(%esp)
 593:	e8 a4 fd ff ff       	call   33c <putc>
        putc(fd, c);
 598:	8b 45 e8             	mov    -0x18(%ebp),%eax
 59b:	0f be c0             	movsbl %al,%eax
 59e:	89 44 24 04          	mov    %eax,0x4(%esp)
 5a2:	8b 45 08             	mov    0x8(%ebp),%eax
 5a5:	89 04 24             	mov    %eax,(%esp)
 5a8:	e8 8f fd ff ff       	call   33c <putc>
      }
      state = 0;
 5ad:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 5b4:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
 5b8:	8b 55 0c             	mov    0xc(%ebp),%edx
 5bb:	8b 45 ec             	mov    -0x14(%ebp),%eax
 5be:	8d 04 02             	lea    (%edx,%eax,1),%eax
 5c1:	0f b6 00             	movzbl (%eax),%eax
 5c4:	84 c0                	test   %al,%al
 5c6:	0f 85 6e fe ff ff    	jne    43a <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 5cc:	c9                   	leave  
 5cd:	c3                   	ret    
 5ce:	90                   	nop
 5cf:	90                   	nop

000005d0 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 5d0:	55                   	push   %ebp
 5d1:	89 e5                	mov    %esp,%ebp
 5d3:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 5d6:	8b 45 08             	mov    0x8(%ebp),%eax
 5d9:	83 e8 08             	sub    $0x8,%eax
 5dc:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 5df:	a1 14 08 00 00       	mov    0x814,%eax
 5e4:	89 45 fc             	mov    %eax,-0x4(%ebp)
 5e7:	eb 24                	jmp    60d <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 5e9:	8b 45 fc             	mov    -0x4(%ebp),%eax
 5ec:	8b 00                	mov    (%eax),%eax
 5ee:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 5f1:	77 12                	ja     605 <free+0x35>
 5f3:	8b 45 f8             	mov    -0x8(%ebp),%eax
 5f6:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 5f9:	77 24                	ja     61f <free+0x4f>
 5fb:	8b 45 fc             	mov    -0x4(%ebp),%eax
 5fe:	8b 00                	mov    (%eax),%eax
 600:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 603:	77 1a                	ja     61f <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 605:	8b 45 fc             	mov    -0x4(%ebp),%eax
 608:	8b 00                	mov    (%eax),%eax
 60a:	89 45 fc             	mov    %eax,-0x4(%ebp)
 60d:	8b 45 f8             	mov    -0x8(%ebp),%eax
 610:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 613:	76 d4                	jbe    5e9 <free+0x19>
 615:	8b 45 fc             	mov    -0x4(%ebp),%eax
 618:	8b 00                	mov    (%eax),%eax
 61a:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 61d:	76 ca                	jbe    5e9 <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 61f:	8b 45 f8             	mov    -0x8(%ebp),%eax
 622:	8b 40 04             	mov    0x4(%eax),%eax
 625:	c1 e0 03             	shl    $0x3,%eax
 628:	89 c2                	mov    %eax,%edx
 62a:	03 55 f8             	add    -0x8(%ebp),%edx
 62d:	8b 45 fc             	mov    -0x4(%ebp),%eax
 630:	8b 00                	mov    (%eax),%eax
 632:	39 c2                	cmp    %eax,%edx
 634:	75 24                	jne    65a <free+0x8a>
    bp->s.size += p->s.ptr->s.size;
 636:	8b 45 f8             	mov    -0x8(%ebp),%eax
 639:	8b 50 04             	mov    0x4(%eax),%edx
 63c:	8b 45 fc             	mov    -0x4(%ebp),%eax
 63f:	8b 00                	mov    (%eax),%eax
 641:	8b 40 04             	mov    0x4(%eax),%eax
 644:	01 c2                	add    %eax,%edx
 646:	8b 45 f8             	mov    -0x8(%ebp),%eax
 649:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 64c:	8b 45 fc             	mov    -0x4(%ebp),%eax
 64f:	8b 00                	mov    (%eax),%eax
 651:	8b 10                	mov    (%eax),%edx
 653:	8b 45 f8             	mov    -0x8(%ebp),%eax
 656:	89 10                	mov    %edx,(%eax)
 658:	eb 0a                	jmp    664 <free+0x94>
  } else
    bp->s.ptr = p->s.ptr;
 65a:	8b 45 fc             	mov    -0x4(%ebp),%eax
 65d:	8b 10                	mov    (%eax),%edx
 65f:	8b 45 f8             	mov    -0x8(%ebp),%eax
 662:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 664:	8b 45 fc             	mov    -0x4(%ebp),%eax
 667:	8b 40 04             	mov    0x4(%eax),%eax
 66a:	c1 e0 03             	shl    $0x3,%eax
 66d:	03 45 fc             	add    -0x4(%ebp),%eax
 670:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 673:	75 20                	jne    695 <free+0xc5>
    p->s.size += bp->s.size;
 675:	8b 45 fc             	mov    -0x4(%ebp),%eax
 678:	8b 50 04             	mov    0x4(%eax),%edx
 67b:	8b 45 f8             	mov    -0x8(%ebp),%eax
 67e:	8b 40 04             	mov    0x4(%eax),%eax
 681:	01 c2                	add    %eax,%edx
 683:	8b 45 fc             	mov    -0x4(%ebp),%eax
 686:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 689:	8b 45 f8             	mov    -0x8(%ebp),%eax
 68c:	8b 10                	mov    (%eax),%edx
 68e:	8b 45 fc             	mov    -0x4(%ebp),%eax
 691:	89 10                	mov    %edx,(%eax)
 693:	eb 08                	jmp    69d <free+0xcd>
  } else
    p->s.ptr = bp;
 695:	8b 45 fc             	mov    -0x4(%ebp),%eax
 698:	8b 55 f8             	mov    -0x8(%ebp),%edx
 69b:	89 10                	mov    %edx,(%eax)
  freep = p;
 69d:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6a0:	a3 14 08 00 00       	mov    %eax,0x814
}
 6a5:	c9                   	leave  
 6a6:	c3                   	ret    

000006a7 <morecore>:

static Header*
morecore(uint nu)
{
 6a7:	55                   	push   %ebp
 6a8:	89 e5                	mov    %esp,%ebp
 6aa:	83 ec 28             	sub    $0x28,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 6ad:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 6b4:	77 07                	ja     6bd <morecore+0x16>
    nu = 4096;
 6b6:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 6bd:	8b 45 08             	mov    0x8(%ebp),%eax
 6c0:	c1 e0 03             	shl    $0x3,%eax
 6c3:	89 04 24             	mov    %eax,(%esp)
 6c6:	e8 59 fc ff ff       	call   324 <sbrk>
 6cb:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(p == (char*)-1)
 6ce:	83 7d f0 ff          	cmpl   $0xffffffff,-0x10(%ebp)
 6d2:	75 07                	jne    6db <morecore+0x34>
    return 0;
 6d4:	b8 00 00 00 00       	mov    $0x0,%eax
 6d9:	eb 22                	jmp    6fd <morecore+0x56>
  hp = (Header*)p;
 6db:	8b 45 f0             	mov    -0x10(%ebp),%eax
 6de:	89 45 f4             	mov    %eax,-0xc(%ebp)
  hp->s.size = nu;
 6e1:	8b 45 f4             	mov    -0xc(%ebp),%eax
 6e4:	8b 55 08             	mov    0x8(%ebp),%edx
 6e7:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 6ea:	8b 45 f4             	mov    -0xc(%ebp),%eax
 6ed:	83 c0 08             	add    $0x8,%eax
 6f0:	89 04 24             	mov    %eax,(%esp)
 6f3:	e8 d8 fe ff ff       	call   5d0 <free>
  return freep;
 6f8:	a1 14 08 00 00       	mov    0x814,%eax
}
 6fd:	c9                   	leave  
 6fe:	c3                   	ret    

000006ff <malloc>:

void*
malloc(uint nbytes)
{
 6ff:	55                   	push   %ebp
 700:	89 e5                	mov    %esp,%ebp
 702:	83 ec 28             	sub    $0x28,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 705:	8b 45 08             	mov    0x8(%ebp),%eax
 708:	83 c0 07             	add    $0x7,%eax
 70b:	c1 e8 03             	shr    $0x3,%eax
 70e:	83 c0 01             	add    $0x1,%eax
 711:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((prevp = freep) == 0){
 714:	a1 14 08 00 00       	mov    0x814,%eax
 719:	89 45 f0             	mov    %eax,-0x10(%ebp)
 71c:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 720:	75 23                	jne    745 <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 722:	c7 45 f0 0c 08 00 00 	movl   $0x80c,-0x10(%ebp)
 729:	8b 45 f0             	mov    -0x10(%ebp),%eax
 72c:	a3 14 08 00 00       	mov    %eax,0x814
 731:	a1 14 08 00 00       	mov    0x814,%eax
 736:	a3 0c 08 00 00       	mov    %eax,0x80c
    base.s.size = 0;
 73b:	c7 05 10 08 00 00 00 	movl   $0x0,0x810
 742:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 745:	8b 45 f0             	mov    -0x10(%ebp),%eax
 748:	8b 00                	mov    (%eax),%eax
 74a:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(p->s.size >= nunits){
 74d:	8b 45 ec             	mov    -0x14(%ebp),%eax
 750:	8b 40 04             	mov    0x4(%eax),%eax
 753:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 756:	72 4d                	jb     7a5 <malloc+0xa6>
      if(p->s.size == nunits)
 758:	8b 45 ec             	mov    -0x14(%ebp),%eax
 75b:	8b 40 04             	mov    0x4(%eax),%eax
 75e:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 761:	75 0c                	jne    76f <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 763:	8b 45 ec             	mov    -0x14(%ebp),%eax
 766:	8b 10                	mov    (%eax),%edx
 768:	8b 45 f0             	mov    -0x10(%ebp),%eax
 76b:	89 10                	mov    %edx,(%eax)
 76d:	eb 26                	jmp    795 <malloc+0x96>
      else {
        p->s.size -= nunits;
 76f:	8b 45 ec             	mov    -0x14(%ebp),%eax
 772:	8b 40 04             	mov    0x4(%eax),%eax
 775:	89 c2                	mov    %eax,%edx
 777:	2b 55 f4             	sub    -0xc(%ebp),%edx
 77a:	8b 45 ec             	mov    -0x14(%ebp),%eax
 77d:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 780:	8b 45 ec             	mov    -0x14(%ebp),%eax
 783:	8b 40 04             	mov    0x4(%eax),%eax
 786:	c1 e0 03             	shl    $0x3,%eax
 789:	01 45 ec             	add    %eax,-0x14(%ebp)
        p->s.size = nunits;
 78c:	8b 45 ec             	mov    -0x14(%ebp),%eax
 78f:	8b 55 f4             	mov    -0xc(%ebp),%edx
 792:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 795:	8b 45 f0             	mov    -0x10(%ebp),%eax
 798:	a3 14 08 00 00       	mov    %eax,0x814
      return (void*)(p + 1);
 79d:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7a0:	83 c0 08             	add    $0x8,%eax
 7a3:	eb 38                	jmp    7dd <malloc+0xde>
    }
    if(p == freep)
 7a5:	a1 14 08 00 00       	mov    0x814,%eax
 7aa:	39 45 ec             	cmp    %eax,-0x14(%ebp)
 7ad:	75 1b                	jne    7ca <malloc+0xcb>
      if((p = morecore(nunits)) == 0)
 7af:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7b2:	89 04 24             	mov    %eax,(%esp)
 7b5:	e8 ed fe ff ff       	call   6a7 <morecore>
 7ba:	89 45 ec             	mov    %eax,-0x14(%ebp)
 7bd:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 7c1:	75 07                	jne    7ca <malloc+0xcb>
        return 0;
 7c3:	b8 00 00 00 00       	mov    $0x0,%eax
 7c8:	eb 13                	jmp    7dd <malloc+0xde>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 7ca:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7cd:	89 45 f0             	mov    %eax,-0x10(%ebp)
 7d0:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7d3:	8b 00                	mov    (%eax),%eax
 7d5:	89 45 ec             	mov    %eax,-0x14(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 7d8:	e9 70 ff ff ff       	jmp    74d <malloc+0x4e>
}
 7dd:	c9                   	leave  
 7de:	c3                   	ret    
