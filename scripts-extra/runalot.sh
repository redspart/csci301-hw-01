#!/bin/bash

for ((n=0;n<500;n++))
do
   echo $n
   
   python process-run.py -s $n -l 3:50,3:50 -c -p | grep "Stats:" | tr '\n' ','  >> noflg.txt
   echo $n  >> noflg.txt
   
   python process-run.py -s $n -l 3:50,3:50 -I IO_RUN_IMMEDIATE -c -p | grep "Stats:" |tr '\n' ',' >> flgim.txt
   echo $n >> flgim.txt
   
   python process-run.py -s $n -l 3:50,3:50 -I IO_RUN_LATER -c -p | grep "Stats:" | tr '\n' ',' >> flglater.txt
   echo $n >> flglater.txt
   
   python process-run.py -s $n -l 3:50,3:50 -S SWITCH_ON_IO -c -p | grep "Stats:" | tr '\n' ',' >> flgIO.txt
   echo $n >> flgIO.txt
   
   python process-run.py -s $n -l 3:50,3:50 -S SWITCH_ON_END -c -p | grep "Stats:" | tr '\n' ',' >> flgEnd.txt
   echo $n >> flgEnd.txt
done
